#include <stdio.h>
int main()
{
    FILE *fp;
    char c;
    printf("File Handling\n");
    fp = fopen("input.dat", "w");
    while((c=getchar())!= EOF)
    fputc(c, fp);
    fclose(fp);
    printf("Data Entered:\n");
    fp = fopen("input.dat", "r");
    while((c=getc(fp))!= EOF)
    putchar(c);
    fclose(fp);
    return 0;
}